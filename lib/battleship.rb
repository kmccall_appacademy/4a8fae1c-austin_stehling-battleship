class BattleshipGame

  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Jeff"), board = Board.random)
    @board = board
    @player = player
    @hit = false
  end

  def game_over?
    board.won?
  end

  def count
    board.count
  end

  def attack(pos)
    if board[pos] == :s
      @hit = true
    else
      @hit = false
    end
    board[pos] = :x
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end


end

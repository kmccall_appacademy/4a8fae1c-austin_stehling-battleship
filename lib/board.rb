class Board

  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10)}
  end

  def self.random
    Board.new(Board.default_grid, true)
  end

  def initialize(grid = Board.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def count
    grid.flatten.select {|ship| ship == :s}.size
  end

  def[](pos)
    row, col = pos
    grid[row][col]
  end

  def[]=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    grid.flatten.include?(nil) == false
  end

  def in_range?(pos)
    pos.all? { |x| x.between?(0, grid.length - 1) }
  end

  def won?
    grid.flatten.include?(:s) == false
  end

  def place_random_ship
    raise "error" if full?
    pos = random_pos

    until empty?(pos)
      pos = random_pos
    end

    self[pos] = :s
  end

  def randomize(count = 17)
    count.times { place_random_ship }
  end

  def random_pos
    [rand(grid.size), rand(grid.size)]
  end

end
